fetch('https://api.chucknorris.io/jokes/random')
    .then((response) => { return response.json() })
    .then((response) => console.log(response));

let timerId;

function loadJoke() {
    clearTimeout(timerId);
    
    fetch('https://api.chucknorris.io/jokes/random')
        .then((response) => { return response.json() })
        .then((response) => { 
            let jokeDiv = document.getElementById("joke");
            jokeDiv.innerText = response.value;
        });
}

loadJoke();

document.getElementById("joke-btn").onclick = () => { 
    loadJoke();
    autoRefresh();
};

function autoRefresh() {
    timerId = setTimeout(() => { loadJoke(); autoRefresh(); }, 10000)
}

autoRefresh();

  let w = $(window);
  let stripes = $("#stripes")
  let fired = false;

  let scroll = () => {
  if (!fired) {
        if (w.scrollTop() + w.height() > stripes.offset().top + 200){
            $('.progress-bar').each((i, e) => {
                $(e).css('width', $(e).data('width') + '%');
            });

            fired = true;
        }
    }
  };
  scroll();
  w.scroll(scroll);

